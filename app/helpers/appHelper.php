<?php

class appHelper {

	public static function mask($content,$mask='') {

		switch ($mask) {
			case 'phone':

				if ( empty($content) )
					$content = '';

				else {
					$total = strlen($content);

					$ddd = substr($content,0,2); // 31

					( $total == 10 ) ? // N8364 where N could be a number
						$prefix = substr($content, 2, 4) :
						$prefix = substr($content, 2, 5);

					$number = substr($content, -4); // 5656

					$content = '('.$ddd.') '.$prefix.'-'.$number;
				}

				break;

			case 'birth_date':

				if ( !empty($content) && $content != '0000-00-00' ) {

					$content = date_format(date_create($content), 'd/m/Y');

				} else
					$content = '';

				break;

			default:

				break;
		}

		return $content;

	}

	public static function email($email='') {

		if ( !empty($email) ) {

			return filter_var($email, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $email);

		} else
			return false;

	}

	public static function pre_rules_name($name='') {

		// $name = str_replace('----------', '', $name);

		// if ( empty($name) )
		// 	return '.';

		return $name;

	}

	public static function splitNames($name,$position='firstname',$email) {

		if ( isset($name) && !empty($name) ) {

			// $name = iconv(mb_detect_encoding($name, mb_detect_order(), true), "UTF-8", $name);
			// $name = mb_convert_encoding($name, "UTF-8");
			// $name = preg_replace('/(?!\n)[\p{Cc}]/', '', $name);
			// $name = preg_replace('/[^\p{L}\s]/u', '', $name);
			// $name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $name);
			// $name = filter_var($name, FILTER_UNSAFE_RAW, FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_STRIP_HIGH);

			$name = trim($name);
			$name = str_replace('----------', '', $name);
			$name = str_replace('??', '', $name);
			if ( $name == $email )
				return '.';

			if ( mb_detect_encoding($name) != 'UTF-8' )
				$name = utf8_encode($name);

			$name = strtolower($name);
			$name = str_replace('@','',$name);
			$name = explode(' ', $name);

			( isset($name[0]) && !empty($name[0]) ) ?
				$first_name = trim(ucfirst($name[0])) :
				$first_name = '.';

			if ( isset($name[1]) && !empty($name[1]) ) {

				$last_name = '';

				$total_names = count($name);
				if ( $total_names > 1 ) {
					for ( $l=1;$l<$total_names;++$l )
						$last_name .= trim(ucfirst($name[$l])).' ';
				}

				$last_name = trim($last_name);

			} else
				$last_name = '.';

			// $first_name = self::remove_bs( filter_var($first_name, FILTER_UNSAFE_RAW, FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_STRIP_HIGH) );
			// $last_name = self::remove_bs( filter_var($last_name, FILTER_UNSAFE_RAW, FILTER_FLAG_ENCODE_LOW|FILTER_FLAG_STRIP_HIGH) );

			if ( $position == 'firstname' )
				return utf8_decode( $first_name );
			elseif ( $position == 'lastname' )
				return utf8_decode( $last_name );

		} else
			return '.';

	}

	public static function birthDate($year='') {

		$year = $year+0;

		( !empty($year) ) ?
			$date = $year.'-01-01' :
			$date = '1900-01-01';

		return $date;

	}

	public static function gender($gender='') {

		if ( empty($gender) )
			return '';

		if ( trim($gender) == 'm' || trim($gender) == 'f' )
			return trim($gender);

	}

	public static function is_generic_domain($domain) {
		if ( preg_match("/(gmail|hotmail|yahoo|outlook)/", $domain) )
			return true;
		else
			return false;
	}

	public static function country($country='',$email) {

		if ( !empty($country) ) {
			$country = strtoupper($country);

		} else {

			// Se PAIS vazio, pega dominio do email e verifica se este dominio existe na lista de países e pega o código do país
			$email = explode('@', $email);
			if ( isset($email[1]) && !empty($email[1]) ) {
				$domain = trim($email[1]);

				if ( self::is_generic_domain($domain) == true )
					return '';

				// Se o $country estiver vazio, procura na lista de países o código igual à última parte do domínio
				$country = self::getCountryBasedOnLastDomainPart($domain,'code');

				// Casos de exemplo: fdf2110@tc.columbia.edu
				// Com o LIKE ele pesquisa qualquer coisa que contenha tc.columbia.edu mas não retorna nada pois não tem o tc. em lugar nenhum
				// A funcao abaixo quebra este dominio, verifica se tem mais de duas partes e pega só as duas ultimas
				$subdomain = explode('.',$domain);
				if ( isset($subdomain[0]) && !empty($subdomain[0]) && count($subdomain) > 2 ) {
					$total_domains = count($subdomain);
					$last_domain = $subdomain[$total_domains-1];
					$pre_last_domain = $subdomain[$total_domains-2];
					$domain = $pre_last_domain.'.'.$last_domain;
				}

				// Se PAIS ainda vazio, procura pelo dominio nas listas disponíveis
				// Medias
				$query = Media::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($query) && !empty($query) && empty($country) )
					$country = $query->region;

				// Multilaterais
				$query = Multilateral::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($query) && !empty($query) && empty($country) )
					$country = $query->region;

				// Privates
				$query = PrivateCompany::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($query) && !empty($query) && empty($country) )
					$country = $query->region;

				// Universities
				$query = University::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($query) && !empty($query) && empty($country) )
					$country = $query->region;

				// print_r(DB::getQueryLog());

			}

		}

		return $country;

	}

	public static function getCountryInfoByCode($code,$info='',$email) {

		if ( !empty($code) && strlen($code) == 2 ) {
			$code = strtoupper($code);

			$country = Country::where('code','=',$code)->select(array('name','domain','language'))->first();
			if ( !$country || empty($country) )
				return '';

			switch ( $info ) {
				case 'name':
					$country = $country->name;
					break;

				case 'domain':
					$country = $country->domain;
					break;

				case 'language':
					$country = $country->language;
					break;
			}

		} else {

			$country = '';

			// Se CODE vazio, pega dominio do email e verifica este dominio nas listas disponíveis
			$email = explode('@', $email);
			if ( isset($email[1]) && !empty($email[1]) ) {
				$domain = trim($email[1]);

				// Procura pelo código do país
				// Medias
				$details = Media::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($details) && !empty($details) && empty($country) )
					$country = $details->region;

				// Multilaterais
				$details = Multilateral::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($details) && !empty($details) && empty($country) )
					$country = $details->region;

				// Privates
				$details = PrivateCompany::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($details) && !empty($details) && empty($country) )
					$country = $details->region;

				// Universities
				$details = University::where('domain','like','%'.$domain.'%')->select('region')->first();
				if ( isset($details) && !empty($details) && empty($country) )
					$country = $details->region;

				if ( !empty($country) ) {

					$country_details = Country::where('code','=',$country)->select(array('name','domain','language'))->first();
					if ( isset($country_details) && !empty($country_details) ) {
						switch ( $info ) {
							case 'name':
								$the_country = $country_details->name;
								break;

							case 'domain':
								$the_country = $country_details->domain;
								break;

							case 'language':
								$the_country = $country_details->language;
								break;
						}
						$country = $the_country;
					}

				} else
					$country = '';

				// Se o $country ainda estiver vazio, procura na lista de países o código igual à última parte do domínio
				if ( empty($country) )
					$country = self::getCountryBasedOnLastDomainPart($domain,$info);

			} else
				$country = '';

		}

		return $country;

	}

	private static function getCountryBasedOnLastDomainPart($domain,$info) {

		$country = '';

		$domain = trim($domain);
		$domain_country = explode('.',$domain);
		if ( isset($domain_country[0]) && !empty($domain_country[0]) ) {
			$total_parts = count($domain_country);
			if ( $total_parts == 1 )
				$last_part = 0;
			else
				$last_part = $total_parts-1;

			$search_this = strtoupper( $domain_country[$last_part] );

			$country_details = Country::where('code','=',$search_this)->select(array('name','domain','language','code'))->first();
			if ( isset($country_details) && !empty($country_details) ) {
				switch ( $info ) {
					case 'name':
						$the_country = $country_details->name;
						break;

					case 'domain':
						$the_country = $country_details->domain;
						break;

					case 'language':
						$the_country = $country_details->language;
						break;

					case 'code':
						$the_country = $country_details->code;
						break;
				}
				$country = $the_country;
			}

		}

		return $country;

	}

	public static function job_title($job_title='') {

		if ( !empty($job_title) )
			return $job_title;

		return '';

	}

	public static function institutionType($email='') {

		if ( empty($email) )
			return '';

		$email = explode('@', $email);
		if ( isset($email[1]) && !empty($email[1]) ) {

			$domain = trim($email[1]);

			if ( self::is_generic_domain($domain) == true )
				return '';

			// BIA
			// Centro de Investigación / Research Center
			// Gobierno / Government - OK
			// Institución Académica / Academic Institution
			// Medio de comunicación / Media
			// Organización Multilateral / Multilateral organization
			// Organización de la Sociedad Civil / Civil society organization
			// Sector Privado / Private sector

			// Educational
			$find_domain = University::where('domain','like','%'.$domain.'%')->select('name')->first();
			$exceptions = array('fordham.edu');
			// if ( preg_match('/(\.edu)/', $domain) || ( isset($find_domain) && !empty($find_domain) ) )
			if ( ( preg_match('/(\.edu)/', $domain) && !in_array($domain, $exceptions) ) || ( isset($find_domain) && !empty($find_domain) ) )
				return 'Institución Académica / Academic Institution';

			// Multilateral
			$find_domain = Multilateral::where('domain','like','%'.$domain.'%')->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				return 'Organización Multilateral / Multilateral organization';

			// Media
			$find_domain = Media::where('domain','like','%'.$domain.'%')->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				return 'Medio de comunicación / Media';

			// Private sector
			$find_domain = PrivateCompany::where('domain','=',$domain)->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				return 'Sector Privado / Private sector';

			// Government
			if ( preg_match('/\.(gob|gov|gub|jus\.br)/', $domain) )
				return 'Gobierno / Government';

		} else
			return '';

	}

	public static function company($company='',$email) {

		if ( empty($email) )
			return '';

		$email = explode('@', $email);
		if ( isset($email[1]) && !empty($email[1]) ) {

			$domain = trim($email[1]);
			$the_name = '';

			// Search for academic institutions
			$find_domain = University::where('domain','=',$domain)->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				$the_name = $find_domain->name;

			// Multilateral
			$find_domain = Multilateral::where('domain','=',$domain)->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				$the_name = $find_domain->name;

			// Media
			$find_domain = Media::where('domain','=',$domain)->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				$the_name = $find_domain->name;

			// Private
			$find_domain = PrivateCompany::where('domain','=',$domain)->select('name')->first();
			if ( isset($find_domain) && !empty($find_domain) )
				$the_name = $find_domain->name;

			// Re-check if country and country name are filled
			// if ( isset($the_name) && !empty($the_name) )

			return $the_name;

		} else
			return '';

	}

	public static function twitter($twitter='') {

		if ( empty($twitter) )
			return '';

		$twitter = str_replace('@', '', $twitter);
		$twitter = strtolower($twitter);

		return $twitter;

	}

	public static function projects($projects='') {

		if ( !empty($projects) )
			return $projects;

		return '';

	}

	public static function topics($topics='') {

		if ( !empty($topics) )
			return $topics;

		return '';

	}

	public static function phone($phone='') {

		if ( !empty($phone) )
			return $phone;

		return '';

	}

	public static function language($code='') {

		$language = Country::where('code','=',strtoupper($code))->select('language')->first();
		if ( !$language )
			return '';

		$language = $language->language;
		return $language;

	}

}
