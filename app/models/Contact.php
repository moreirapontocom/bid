<?php

// use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Contact extends Eloquent {

	// use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'contacts';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('id');

	protected $guarded = array('id');

	// protected $dates = ['deleted_at'];

}
