<?php

Route::get('/', 'HomeController@showWelcome');
Route::post('upload','HomeController@upload');

Route::post('processFile','HomeController@processFile');
Route::post('unsubscribe','HomeController@unsubscribeFile');

Route::post('exportToFile', 'HomeController@exportToFile');
Route::get('importIntoMailchimp', 'HomeController@importIntoMailchimp');

App::missing(function($exception) {
	return Response::view('404', array(), 404);
});
