<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Basics extends Migration {

	public function up()
	{

		Schema::create('stats', function(Blueprint $table) {
			$table->increments('id');

			// Stats from file importation
			$table->integer('imported')->nullable();
			$table->integer('removed')->nullable();
			$table->integer('unsubscribed')->nullable();

			// Stats from Mailchimp API
			$table->integer('subscribed')->nullable();
			$table->integer('updated')->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		// id, username, name, email, language, location, year_of_birth, gender, level_of_education,
		// mailing_address, goals, city, country
		Schema::create('contacts_import', function(Blueprint $table) {
			$table->string('id')->nullable();
			$table->string('username')->nullable();
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->string('language')->nullable();
			$table->string('location')->nullable();
			$table->string('year_of_birth')->nullable();
			$table->string('gender')->nullable();
			$table->string('level_of_education')->nullable();
			$table->string('mailing_address')->nullable();
			$table->string('goals')->nullable();
			$table->string('city')->nullable();
			$table->string('country')->nullable();
			$table->integer('is_cleaned')->default(0);
			$table->integer('is_optout')->default(0);
			$table->string('origin')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->increments('idk');
			$table->unique(array('idk'));
		});

		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');

			$table->string('email')->nullable();
			$table->string('email_hash')->nullable(); // MD5(strtolower(email)) | For Mailchimp (https://developer.mailchimp.com/documentation/mailchimp/guides/manage-subscribers-with-the-mailchimp-api/)
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->date('birth_date')->nullable();
			$table->char('gender',2)->nullable();
			$table->string('country')->nullable();
			$table->string('country_name')->nullable();
			$table->string('job_title')->nullable();
			$table->string('institution_type')->nullable();
			$table->string('company')->nullable();
			$table->string('origin')->nullable();
			$table->string('twitter')->nullable();
			$table->string('projects')->nullable();
			$table->string('topics')->nullable();
			$table->string('phone')->nullable();
			$table->string('language')->nullable();
			$table->integer('is_synced')->default(0);

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		Schema::create('countries', function(Blueprint $table) {
			$table->increments('id');

			$table->string('code');
			$table->string('name');
			$table->string('domain')->nullable();
			$table->string('language')->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		Schema::create('universities', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name');
			$table->string('domain');
			$table->char('region',2)->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		Schema::create('multilaterais', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name');
			$table->string('domain');
			$table->char('region',2)->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		Schema::create('privates', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name');
			$table->string('domain');
			$table->char('region',2)->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});

		Schema::create('medias', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name');
			$table->string('domain');
			$table->char('region',2)->nullable();

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->nullable();
			$table->timestamp('deleted_at')->nullable();
			$table->unique(array('id'));
		});
	}

	public function down()
	{
		Schema::drop('stats');
		Schema::drop('contacts_import');
		Schema::drop('contacts');
		Schema::drop('countries');
		Schema::drop('universities');
		Schema::drop('multilaterais');
		Schema::drop('privates');
		Schema::drop('medias');
	}

}
