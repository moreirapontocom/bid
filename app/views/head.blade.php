<meta charset="UTF-8">

<link type="text/css" rel="stylesheet" href="{{ asset('packages/font-awesome/css/font-awesome.min.css') }}">
<link type="text/css" rel="stylesheet" href="{{ asset('packages/bootstrap/dist/css/bootstrap.min.css') }}">

<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

<style>
	.logo {
		margin: 40px 0;
		margin-bottom: 10px;
		border-bottom: solid 1px #efefef;
		padding-bottom: 10px;
	}
	footer {
		margin: 50px 0;
		border-top: solid 1px #efefef;
		padding-top: 40px;
		font-size: .9em;
		color: #999;
	}
	.alert {
	    position: fixed;
	    top: 0;
	    left: 0;
	    width: 100%;
	    margin: 0;
	    padding: 15px 0;
	    -webkit-border-radius: 0;
	    -moz-border-radius: 0;
	    border-radius: 0;
	    border: none;
	    transition: all .2s ease-in-out;
	    -moz-transition: all .2s ease-in-out;
	}

	.alert-hidden {
	    top: -200px;
	    transition: all .2s ease-in-out;
	    -moz-transition: all .2s ease-in-out;
	}

	.alert-danger {
	    background-color: #e53935;
	    color: #fff;
	}

	.alert-success {
	    background-color: #4caf50;
	    color: #fff;
	}

	.table-responsive {
		width: 100%;
		margin-bottom: 15px;
		overflow-y: hidden;
		-ms-overflow-style: -ms-autohiding-scrollbar;
		border: 1px solid #ddd;
	}
	.table-responsive > .table {
		margin-bottom: 0;
	}
	.table-responsive > .table > thead > tr > th,
	.table-responsive > .table > tbody > tr > th,
	.table-responsive > .table > tfoot > tr > th,
	.table-responsive > .table > thead > tr > td,
	.table-responsive > .table > tbody > tr > td,
	.table-responsive > .table > tfoot > tr > td {
		white-space: nowrap;
	}
	.table-responsive > .table-bordered {
		border: 0;
	}
	.table-responsive > .table-bordered > thead > tr > th:first-child,
	.table-responsive > .table-bordered > tbody > tr > th:first-child,
	.table-responsive > .table-bordered > tfoot > tr > th:first-child,
	.table-responsive > .table-bordered > thead > tr > td:first-child,
	.table-responsive > .table-bordered > tbody > tr > td:first-child,
	.table-responsive > .table-bordered > tfoot > tr > td:first-child {
		border-left: 0;
	}
	.table-responsive > .table-bordered > thead > tr > th:last-child,
	.table-responsive > .table-bordered > tbody > tr > th:last-child,
	.table-responsive > .table-bordered > tfoot > tr > th:last-child,
	.table-responsive > .table-bordered > thead > tr > td:last-child,
	.table-responsive > .table-bordered > tbody > tr > td:last-child,
	.table-responsive > .table-bordered > tfoot > tr > td:last-child {
		border-right: 0;
	}
	.table-responsive > .table-bordered > tbody > tr:last-child > th,
	.table-responsive > .table-bordered > tfoot > tr:last-child > th,
	.table-responsive > .table-bordered > tbody > tr:last-child > td,
	.table-responsive > .table-bordered > tfoot > tr:last-child > td {
		border-bottom: 0;
	}
</style>