<script type="text/javascript" src="{{ asset('packages/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script>
	function openPreview() {
		$('#preview_dataset').removeClass('hidden');
		goTo('#step4');
	}
	function goTo(target) {
		$('html, body').animate({
		    scrollTop: $(target).offset().top
		}, 1000);
	}
</script>

<footer>
	<div class="container">
		&copy; {{ date('Y') }} Inter-American Development Bank. Developed by <a href="https://w3inova.com" target="_blank">W3 Inova</a>.
	</div><!-- /.container -->
</footer>

<!-- Modal upload -->
<div class="modal fade" id="uploadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h1><i class="fa fa-gears"></i></h1>
				<h3>
					Uploading and pre-processing file...
				</h3>
			</div>
		</div>
	</div>
</div>

<!-- Modal start processing -->
<div class="modal fade" id="modalStartProcessing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h1><i class="fa fa-gears"></i></h1>
				<h3>The file is being processed.</h3>
				<p class="text-danger">
					This may take a few minutes. Do not close this window.
				</p>
			</div>
		</div>
	</div>
</div>

<!-- Modal unsubscribe -->
<div class="modal fade" id="unsubscribeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h1><i class="fa fa-trash"></i></h1>
				<h3>
					Removing Contacts...
				</h3>
			</div>
		</div>
	</div>
</div>

<!-- Modal Confirm Mailchimp sync -->
<div class="modal fade" id="confirmMCSyncModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h1><i class="fa fa-warning text-danger"></i></h1>
				<h3>Confirm?</h3>
				<p>
					Are you really want to proceed to the sync with MailChimp account?<br>
					All new contacts will be inserted on MailChimp automatically.
				</p>
				<br><br>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<a href="{{ url('importIntoMailchimp') }}" class="btn btn-success" onclick="$('#syncModal, #confirmMCSyncModal').modal('toggle')"><i class="fa fa-check"></i> Proceed</a>
			</div>
		</div>
	</div>
</div>

<!-- Modal Sync with Mailchimp -->
<div class="modal fade" id="syncModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<h1><img src="{{ asset('img/sync.fw.png') }}" alt=""></h1>
				<h3>Syncronizing contacts...</h3>
			</div>
		</div>
	</div>
</div>

@if ( count($errors) > 0 )
	<script type="text/javascript">
		$(function() {
			$('.alert').click(function() {
				close_warning();
			});
		});
	</script>
	<div class="alert alert-danger">
		<div class="container">
			<div class="text-right">
				<small>
					<i class="fa fa-times"></i> fechar
				</small>
			</div>
			<ul class="list-unstyled">
				@foreach ( $errors->all() as $error )
					<li>&mdash; {{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif

@if ( Session::has('alert') )
	<script type="text/javascript">
		$(function() {
			$('.alert').click(function() {
				close_warning();
			});

			setTimeout('close_warning()', 5000);
		});
	</script>
	<div class="alert alert-success">
		<div class="container">
			<div class="text-right">
				<small>
					<i class="fa fa-times"></i> fechar
				</small>
			</div>
			{{ Session::get('alert') }}
		</div>
	</div>
@endif
