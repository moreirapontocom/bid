<!doctype html>
<html lang="en">
<head>
	<title>BID</title>

	@include('head')

</head>
<body>

	{{--*/ $total_contacts = Contact::count(); /*--}}
	{{--*/ $total_contacts_imported = ContactImported::count(); /*--}}

	<div class="container">

		<center>
			<img src="{{ asset('img/bid-logo-hd.jpg') }}" alt="BID" class="logo" style="max-width: 200px;">
			<br>
			System of Data Normalization - MOOCs datasets
		</center>

		<br><br>

		<h4>Step 1 <span class="text-muted">(of 5)</span></h4>
		<div class="well">

			@if ( Session::has('processThisNumber') )
				{{--*/ $processThisNumber = Session::get('processThisNumber') /*--}}
			@else
				{{--*/ $processThisNumber = 4000; /*--}}
			@endif

			<b>Processing max entries</b><br>
			Inform how many entries should be processed.<br>
			Considering the size of the file, we recommend the standard {{ $processThisNumber }} contacts at a time.
			<br><br>
			<div class="row">
				<div class="col col-lg-2">
					<input type="number" id="process-this" value="{{ $processThisNumber }}" class="form-control">
				</div>
			</div>

			<hr>

			<form method="post" onsubmit="return openUploadingModal()" action="{{ url('upload') }}#step2" accept-charset="UTF-8" enctype="multipart/form-data">

				<!-- Used just to save the session to next processing -->
				<input type="hidden" name="process_max" class="quantity-to-process" value="{{ $processThisNumber }}">

				<h4>Upload file</h4>

				<ol>
					<li>
						Select a XLS or CSV file to be imported. Obs.: The file must follow the patter according to the template <a href="{{ asset('Sample-Contacts-file.csv') }}" target="_blank">available here</a>.
					</li>
					<li>
						Click in "Upload File"
					</li>
					<li>
						You will be directed to step 2.
					</li>
				</ol>

				<div class="row">
					<div class="col col-lg-5">

						<p>
							<label>Select a file to import</label>
							<div class="input-group">
								<input type="text" class="form-control" id="the_file_fake_name" placeholder="Select a file to import">
								<span class="input-group-btn">
									<button class="btn btn-default btn-open-file-browser" type="button"><i class="fa fa-search"></i> Search file</button>
								</span>
							</div><!-- /input-group -->
							<input type="file" name="the_file" id="the_file" class="hidden">
						</p>

					</div><!-- /.col -->
					<div class="col col-lg-3">

						<p>
							<label>&nbsp;</label>
							<div>
								<button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload file @if ( $total_contacts > 0 || $total_contacts_imported > 0 ) (It will remove existent cleaned contacts) @endif</button>
							</div>
						</p>

					</div><!-- /.col -->
				</div><!-- /.row -->

			</form>

		</div><!-- /.well -->








		<!-- Step 2 -->


		<br>

		<hr>

		<br>

		<h4 id="step2">Step 2 <span class="text-muted">(of 5)</span></h4>

		<div class="well">

			<h4>Process file</h4>

			<ol>
				<li>
					Include the course title in the "Contacts Origin" field. The name MUST follow the following pattern: MOOC "Course Title", underline, "Course Edition", underline, "Course Year", e.g., "MOOC Development Project Management IDBx_IDB6x_2015" or "MOOC Mejores Trabajos Mejores Pensiones_v1_IDB2x_2014".
				</li>
				<li>
					Click in "Process Contacts”
				</li>
				<li>
					This step will apply the standardization rules and it may take some time depending on how many contacts are to be processed.
				</li>
			</ol>

			<br>

			@if ( $total_contacts_imported == 0 && $total_contacts == 0 )

				You must execute the previous step first.

			@elseif ( ( $total_contacts_imported == 0 && $total_contacts > 0 ) || ( ContactImported::where('is_cleaned','=',1)->count() > 0 ) )

				All contacts were cleaned.<br>
				Proceed to next step.

			@endif

			<form method="post" action="{{ url('processFile') }}#step2" onsubmit="$('#modalStartProcessing').modal('show')">

				<input type="hidden" class="quantity-to-process" name="process_max" value="{{ $processThisNumber }}">

				@if ( ContactImported::where('is_cleaned','=',0)->count() > 0 )

					<div class="row">
						<div class="col col-lg-3">

							@if ( Session::has('the_origin') )
								{{--*/ $the_origin = Session::get('the_origin') /*--}}
							@else
								{{--*/ $the_origin = ''; /*--}}
							@endif

							<p>
								<label>Contacts origin</label>
								<div>
									<input type="text" class="form-control" name="origin" placeholder="Origin" value="{{ $the_origin }}">
								</div>
							</p>

						</div><!-- /.col -->
						<div class="col col-lg-4">

							<p>
								<label>Contacts to process</label> <span class="text-muted">&mdash; {{ $importedContacts }} contacts not cleaned</span>
								<div>
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-gears"></i> Process {{ $total_contacts_imported }} Contacts ( <span class="text_process_this_number">{{ $processThisNumber }}</span> each time )
									</button>
								</div>
							</p>

						</div><!-- /.col -->
					</div><!-- /.row -->

				@endif

			</form>

		</div><!-- /.well -->
		<!-- /Step2 -->








		<br>

		<hr>

		<br>

		<h4 id="step3">Step 3 <span class="text-muted">(of 5)</span></h4>
		<div class="well">

			<h4 id="contacts">Upload contacts to unsubscribe</h4>

			<ol>
				<li>To unsubscribe contacts from the normalized list, import a .csv file containing the contacts to be removed.</li>
				<li>The list must have at least an "email" column.</li>
			</ol>

			<br>

			@if ( $total_contacts == 0 && $total_contacts_imported == 0 )

				You must execute the previous steps first.

			@elseif ( ContactImported::where('is_cleaned','=',0)->count() > 0 )

				All contacts should be processed before the subscription process.

			@elseif ( ContactImported::where('is_cleaned','=',0)->count() == 0 )

				Contacts are ready for unsubscription process.<br>
				Proceed to next step.

			@endif

			<form method="post" onsubmit="return openUploadingModal()" action="{{ url('upload') }}#step4" accept-charset="UTF-8" enctype="multipart/form-data">

				@if ( $total_contacts_imported == 0 && $total_contacts > 0 )

					<input type="hidden" class="quantity-to-process" name="process_max" value="{{ $processThisNumber }}">
					<input type="hidden" name="upload_to_remove" value="remove">

					<div class="row">
						<div class="col col-lg-5">

							<p>
								<label>Select a file to import</label>
								<div class="input-group">
									<input type="text" class="form-control" id="the_file_fake_name_optout" placeholder="Select a file to import">
									<span class="input-group-btn">
										<button class="btn btn-default btn-open-file-browser-optout" type="button"><i class="fa fa-search"></i> Search file</button>
									</span>
								</div><!-- /input-group -->
								<input type="file" name="the_file" id="the_file_optout" class="hidden">
							</p>

						</div><!-- /.col -->
						<div class="col col-lg-3">

							<p>
								<label>&nbsp;</label>
								<div>
									<button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload file</button>
								</div>
							</p>

						</div><!-- /.col -->
					</div><!-- /.row -->

				@endif

			</form>

		</div><!-- /.well -->







		<br>

		<hr>

		<br>

		<h4 id="step4">Step 4 <span class="text-muted">(of 5)</span></h4>
		<div class="well">

			<h4>Unsubscribe</h4>

			<ol>
				<li>Click at "Unsubscribe Contacts".</li>
			</ol>

			<br>

			@if ( ContactImported::where('is_cleaned','=',1)->count() == 0 || $total_contacts == 0 )

				You must execute the previous steps first.

			@endif

			<form method="post" onsubmit="return openUnsubscribeModal()" action="{{ url('unsubscribe') }}#step5">
				<input type="hidden" class="quantity-to-process" name="process_max" value="{{ $processThisNumber }}">

				@if ( ContactImported::where('is_cleaned','=',1)->count() > 0 )

					<button type="submit" class="btn btn-primary"><i class="fa fa-gears"></i> Unsubscribe {{ $total_contacts_imported }} Contacts ( <span class="text_process_this_number">{{ $processThisNumber }}</span> each time )</button>

				@endif
			</form>

		</div><!-- /.well -->





		<!-- Step 4 -->

		<br>

		<hr>

		<br>

		<h4 id="step5">Step 5 <span class="text-muted">(of 5)</span></h4>
		<div class="well">

			<h4 id="contacts">Preview, download or synchronize the normalized dataset</h4>

			<ol>
				<li>
					Review the status of the dataset in the Summary bellow and in the preview as deems necessary.
				</li>
				<li>
					If everything is in accordance with your expectations, choose between the options to export the dataset as a .CSV file or to sent it directly to MailChimp.
				</li>
			</ol>

			<br>

			@if ( $total_contacts == 0 )

				You must execute the previous steps first.

			@else

				<div class="row">
					<div class="col col-lg-4">
						<a href="javascript:void(null)" class="btn btn-primary btn-block" onclick="$('#modalPreviewDataset').modal('show')">
							<i class="fa fa-eye"></i> Preview dataset
						</a>
					</div>

					<div class="col col-lg-4">

						@if ( Contact::where('is_downloaded','=',0)->count() > 0 )
							<form method="post" action="{{ url('exportToFile') }}">
								<input type="hidden" class="quantity-to-process" name="process_max" value="{{ $processThisNumber }}">
								<button type="submit" class="btn btn-default btn-block">
									<i class="fa fa-file-o"></i> Export to file (.csv) ( Left to download: {{ Contact::where('is_downloaded','=',0)->count() }} )
								</button>
							</form>
						@else
							<button type="button" disabled="disabled" class="btn btn-default btn-block disabled">
								<i class="fa fa-file-o"></i> Export to file (.csv)
							</button>
						@endif

						@if ( Session::has('files_to_download') )

							<hr>

							<b>Download the exported files:</b>

							{{--*/ $files_list = Session::get('files_to_download') /*--}}
							{{--*/ $total_files = count($files_list) /*--}}

							<ul>
								@for ( $i=0;$i<$total_files;++$i )
									<li>
										<a href="{{ $files_list[$i] }}.csv" target="_blank"><i class="fa fa-file"></i> Download file {{ $i }}</a>
									</li>
								@endfor
							</ul>

						@endif

					</div>

					<div class="col col-lg-4">
						@if ( Contact::where('is_synced','=',0)->count() > 0 )
							<button type="button" class="btn btn-link btn-block" data-toggle="modal" data-target="#confirmMCSyncModal">
								<i class="fa fa-exchange"></i> Sync with Mailchimp account ( Left to sync: {{ Contact::where('is_synced','=',0)->count() }} )
							</button>
						@else
							<button type="button" disabled="disabled" class="btn btn-link btn-block disabled">
								<i class="fa fa-exchange"></i> Sync with Mailchimp account
							</button>
						@endif
					</div>
				</div>

				@include('preview-dataset')

				<hr>

				Summary
				<ul>
					@foreach ( $stats as $stat )
						<li>
							<b>Total of contacts imported:</b> {{ $importedContacts + $processedContacts }}
						</li>
						<li>
							<b>Invalids:</b> {{ $stat->removed }}
						</li>
						<li>
							<b>Processed:</b> {{ $processedContacts }} <span class="text-muted">&mdash; left {{ $importedContacts }} to process</span>
						</li>
						<li>
							<b>Unsubscribed:</b> {{ $stat->unsubscribed }}
						</li>
						@if ( isset($stat->subscribed) )
							<li>
								<b>Sent to Mailchimp:</b> {{ $stat->subscribed }}
							</li>
							<li>
								<b>Duplicates:</b> {{ $stat->updated }}
							</li>
						@endif
					@endforeach
				</ul>

			@endif

		</div><!-- /.well -->









	</div><!-- /.container -->

	@include('footer')

	<script>
		$(function() {

			$('#process-this').keyup(function() {
				var _this = $(this),
					input_fields = $('.quantity-to-process'),
					text_fields = $('.text_process_this_number');

				input_fields.val( _this.val() );
				text_fields.html( _this.val() );
			});

			$('.btn-open-file-browser').click(function() {
				$('#the_file').click();
			});
			$('#the_file').change(function() {
				$('#the_file_fake_name').val( $(this).val() );
			});

			$('.btn-open-file-browser-optout').click(function() {
				$('#the_file_optout').click();
			});
			$('#the_file_optout').change(function() {
				$('#the_file_fake_name_optout').val( $(this).val() );
			});
		});

		function openUploadingModal() {
			$('#uploadingModal').modal('show');
		}

		function openUnsubscribeModal() {
			$('#unsubscribeModal').modal('show');
		}

		function close_warning() {
		    $('.alert').addClass('alert-hidden');
		}
	</script>

</body>
</html>
