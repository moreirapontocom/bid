<!-- Modal preview -->
<div class="modal fade" id="modalPreviewDataset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">


				You are previewing the first 200 cleaned contacts.
				<br>
				<span class="text-danger">
					Obs.: In the red lines are contacts that have not yet been synchronized with MailChimp through this system.
				</span>

				<br><br>

				<div class="table-responsive" style="overflow-y: auto;max-height: 500px;">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th class="text-center text-muted">#</th>
								<th style="min-width: 240px;">
									Dirección de correo electrónico<br>
									Email address
								</th>
								<th style="min-width: 110px;">
									Nombre<br>
									First Name
								</th>
								<th style="min-width: 110px;">
									Apellido<br>
									Last Name
								</th>
								<th style="width: 170px;">
									Fecha de Nacimiento<br>
									Date of birth
								</th>
								<th style="width: 80px;" class="text-center">
									Género<br>
									Gender
								</th>
								<th>
									País<br>Country
								</th>
								<th>
									Título o Cargo<br>
									Title or Position
								</th>
								<th>
									Tipo de institución<br>
									Type of institution
								</th>
								<th>
									Empresa o Organización<br>
									Company or Organization
								</th>
								<th>
									Fuente del Contacto<br>
									Source dataset
								</th>
								<th>Twitter</th>
								<th>
									Productos de interés<br>
									Products of interest
								</th>
								<th>
									Temas de interése<br>
									Topics of interest
								</th>
								<th>
									Número de Teléfono<br>
									Phone Number
								</th>
								<th>
									Idioma<br>
									Language
								</th>
							</tr>
						</thead>
						<tbody>

							{{--*/ $i = 1 /*--}}
							@foreach ( $contacts as $item )
								<tr @if ( $item->is_synced == 0 ) class="danger" @endif>
									<td class="text-center text-muted">{{ $item->id }}</td>
									<td>{{ $item->email }}</td>
									<td>{{ $item->first_name }}</td>
									<td>{{ $item->last_name }}</td>
									<td>{{ appHelper::mask( $item->birth_date,'birth_date' ) }}</td>
									<td class="text-center">
										@if ( $item->gender == 'm' )
											Masculino / Male
										@elseif ( $item->gender == 'f' )
											Feminino / Female
										@endif
									</td>
									<td>{{ $item->country_name }}</td>
									<td>{{ $item->job_title }}</td>
									<td>{{ appHelper::institutionType( $item->email ) }}</td>
									<td>{{ $item->company }}</td>
									<td>{{ $item->origin }}</td>
									<td>{{ appHelper::twitter( $item->twitter ) }}</td>
									<td>{{ $item->projects }}</td>
									<td>{{ $item->topics }}</td>
									<td>{{ $item->phone }}</td>
									<td>{{ appHelper::language( $item->country ) }}</td>
								</tr>

								{{--*/ ++$i /*--}}
							@endforeach

						</tbody>
					</table>
				</div><!-- /.table-responsive -->



			</div>
		</div>
	</div>
</div>
