<?php

class HomeController extends BaseController {

	public function showWelcome() {

		$contacts = Contact::orderBy('id','ASC')->take(200)->get();
		$stats = Stat::where('id','=',1)->get();
		$importedContacts = ContactImported::count();
		$processedContacts = Contact::count();

		$info = array(
			'contacts' => $contacts,
			'stats' => $stats,
			'importedContacts' => $importedContacts,
			'processedContacts' => $processedContacts
		);

		return View::make('home', $info);
	}

	public function unsubscribeFile() {

		$formData = Input::all();

		( !isset($formData['process_max']) || empty($formData['process_max']) ) ?
			$process_max = 4000 :
			$process_max = $formData['process_max']+0;

		Session::put('processThisNumber',$process_max);

		$optout_list = 0;

		$results = ContactImported::take($process_max)->get();

		foreach ( $results as $item ) {

			// Using the ID column because on opt-out file has only one column
			if ( !empty($item['id']) ) {
				$optoutUsers[] = $item['id'];
				$optoutUsers_lowercase[] = strtolower( $item['id'] );
			}

		}

		if (
			isset($optoutUsers_lowercase) && !empty($optoutUsers_lowercase) &&
			isset($optoutUsers) && !empty($optoutUsers)
		) {
			$optout_list = Contact::whereIn('email', $optoutUsers_lowercase)->delete();
			ContactImported::whereIn('id',$optoutUsers)->delete();

			$msg = $optout_list.' contacts was opted-out';
		} else
			$msg = 'All contacts already opted-out.';

		$statistics = Stat::where('id','=',1)->first();
		$statistics->unsubscribed = $statistics->unsubscribed + $optout_list;
		$statistics->save();

		return Redirect::back()->with('alert',$msg);

	}

	public function processFile() {

		$formData = Input::all();

		$rules = array(
			'process_max' => 'required',
			'origin' => 'required|min:3|max:50',
		);

		$i18n = array(
			'required'  => 'O campo ":attribute" é obrigatório.',
			'min'       => 'O campo ":attribute" deve ter no mínimo :min caracteres',
			'max'       => 'O campo ":attribute" deve ter no máximo :max caracteres'
		);

		$friendlyNames = array(
			'origin' => 'Contact origin'
		);

		( !isset($formData['process_max']) || empty($formData['process_max']) ) ?
			$process_max = 4000 :
			$process_max = $formData['process_max']+0;

		Session::put('processThisNumber',$process_max);

		( isset($formData['origin']) && !empty($formData['origin']) ) ?
			$the_origin = $formData['origin'] :
			$the_origin = '';

		Session::put('the_origin',$the_origin);

		$stats_invalids = 0;

		$results = ContactImported::take($process_max)->get();

		foreach ( $results as $item ) {

			( isset($item['email']) && !empty($item['email']) ) ?
				$the_email = trim(strtolower($item['email'])) :
				$the_email = '';

			if ( !empty($the_email) && appHelper::email( $the_email ) ) {

				$name = $item['name'];
				$lowercase_email = strtolower( $item['email'] );

				$country_code = appHelper::country( $item['country'], $item['email'] );
				$country_name = appHelper::getCountryInfoByCode( $country_code, 'name', $item['email'] );

				$processed[] = $item['email']; // Should use the original email to found and delete after process

				$newInsert[] = array(
					'email' => $lowercase_email,
					'email_hash' => md5( $lowercase_email ),
					'first_name' => appHelper::splitNames( $name,'firstname',$item['email'] ),
					'last_name' => appHelper::splitNames( $name,'lastname',$item['email'] ),
					'birth_date' => appHelper::birthDate( $item['year_of_birth'] ),
					'gender' => appHelper::gender( $item['gender'] ),
					'country' => $country_code,
					'country_name' => $country_name,
					'job_title' => appHelper::job_title( '' ),
					'institution_type' => appHelper::institutionType( $item['email'] ),
					'company' => appHelper::company( '', $item['email'] ),
					'origin' => $the_origin,
					'twitter' => ( ( isset($item['twitter']) ) ? appHelper::twitter( $item['twitter'] ) : '' ),
					'projects' => appHelper::projects( '' ),
					'topics' => appHelper::topics( '' ),
					'phone' => ( ( isset($item['phone']) ) ? appHelper::phone( $item['phone'] ) : '' ),
					'language' => appHelper::language( $country_code )
				);

			} else
				++$stats_invalids;

		}

		$statistics = Stat::where('id','=',1)->first();
		$statistics->imported = count($newInsert);
		$statistics->removed = $stats_invalids;
		$statistics->save();

		if ( isset($newInsert) && !empty($newInsert) ) {

			$save_imports = new Contact();
			$save_imports->insert( $newInsert );

			ContactImported::whereIn('email', $processed)->delete();

			$msg = count($newInsert).' contacts was processed';
		}

		return Redirect::back()->with('alert',$msg);

	}

	public function upload() {

		DB::table('contacts_import')->truncate();
		Session::forget('files_to_download');

		$formData = Input::all();

		( !isset($formData['process_max']) || empty($formData['process_max']) ) ?
			$process_max = 4000 :
			$process_max = $formData['process_max']+0;

		Session::put('processThisNumber',$process_max);

		$rules = array(
			'the_file' => 'required'
		);

		// Input::file('the_file')->getMimeType()
		// Excel: application/vnd.ms-office
		// CSV (Mac): text/plain
		// CSV (original BID): application/octet-stream

		$i18n = array(
			'required'  => 'O campo ":attribute" é obrigatório.',
			'min'       => 'O campo ":attribute" deve ter no mínimo :min caracteres',
			'max'       => 'O campo ":attribute" deve ter no máximo :max caracteres',
			'mimes' 	=> 'O campo ":attribute" must have a .xls or .csv extension'
		);

		$friendlyNames = array(
			'the_file' => 'The file'
		);

    	$validation = Validator::make($formData, $rules, $i18n);
		$validation->setAttributeNames($friendlyNames);

	    if ( $validation->fails() ) {
	        return Redirect::back()
	            ->withErrors($validation)
	            ->withInput();
	    }

		$supported_mime_types = array('application/vnd.ms-office','text/plain','application/octet-stream');
		$file = Input::file('the_file');
		$mime = $file->getMimeType();
		if ( !in_array($mime, $supported_mime_types) ) {
			return Redirect::back()
	            ->withErrors(array('Formato de arquivo não permitido'));
        }

		// LOAD FILES in MySQL are just allowed on MySQL defined directory
		// This block fetch that local to upload file there
		$DBHost = Config::get('database.connections.'.Config::get('database.default').'.host');
		$DBDatabase = Config::get('database.connections.'.Config::get('database.default').'.database');
		$DBUser = Config::get('database.connections.'.Config::get('database.default').'.username');
		$DBPassword = Config::get('database.connections.'.Config::get('database.default').'.password');

		$con = mysqli_connect($DBHost,$DBUser,$DBPassword) or die (mysql_error());
		mysqli_select_db($con, $DBDatabase) or die (mysql_error());

		# /var/lib/mysql-files/
		$query_priv = mysqli_query($con,'SHOW VARIABLES LIKE "secure_file_priv"') or die (mysql_error());
		while ( $res = mysqli_fetch_array($query_priv) ) {
			$mysql_priv_files = $res['Value'];
		}
		if ( !isset($mysql_priv_files) || empty($mysql_priv_files) )
			die('Impossible to read MySQL secure_file_priv variable');
		//

		if ( !empty($formData['the_file']) && Input::file('the_file')->isValid() ) {

			// $destinationPath = 'uploads';
			$destinationPath = $mysql_priv_files;
			$extension = Input::file('the_file')->getClientOriginalExtension();
			$fileName = rand(11111,99999).'.'.$extension;
			Input::file('the_file')->move($destinationPath, $fileName);

			$msg = '';

			// $sql1 = "SET NAMES 'UTF8'";
			$sql1 = "SET character_set_database=utf8";
			$query1 = mysqli_query($con,$sql1) or die (mysql_error());

			// LOAD DATA INFILE was the only way to import a huge csv file
			$query = "LOAD DATA INFILE '".$mysql_priv_files.$fileName."'
				INTO TABLE contacts_import
				CHARACTER SET latin1
				FIELDS TERMINATED BY ','
				ENCLOSED BY '\"'
				LINES TERMINATED BY '\r\n'
				IGNORE 1 LINES";

			mysqli_query($con,$query) or die (mysql_error());
			// end LOAD

			if ( file_exists($mysql_priv_files.$fileName) )
				unlink( $mysql_priv_files.$fileName );

			ContactImported::where('email')->delete();

			// mysql_close($con,$DBHost);

			if ( isset($formData['upload_to_remove']) && !empty($formData['upload_to_remove']) && $formData['upload_to_remove'] == 'remove' )
				ContactImported::where('is_cleaned','<>',1)->update(array('is_cleaned' => 1));
			else {
				DB::table('contacts')->truncate();
				$statistics = Stat::where('id','=',1)->first();
				$statistics->imported = 0;
				$statistics->removed = 0;
				$statistics->unsubscribed = 0;
				$statistics->subscribed = 0;
				$statistics->updated = 0;
				$statistics->save();
			}

			return Redirect::back()->with('alert','File uploaded. Go to the next step.');

		}

	}

	public function exportToFile() {

		$formData = Input::all();

		( !isset($formData['process_max']) || empty($formData['process_max']) ) ?
			$process_max = 4000 :
			$process_max = $formData['process_max']+0;

		$data = Contact::where('is_downloaded','=',0)
						->orderBy('first_name','ASC')
						->select(
							array(
								DB::raw('id'),
								DB::raw('email AS `Dirección de correo electrónico / Email address`'),
								DB::raw('first_name AS `Nombre / First Name`'),
								DB::raw('last_name AS `Apellido / Last Name`'),
								DB::raw("DATE_FORMAT(birth_date,'%m/%d/%Y') AS `Fecha de Nacimiento / Date of birth`"),
								DB::raw("(CASE gender WHEN 'm' THEN 'Masculino / Male' WHEN 'f' THEN 'Feminino / Female' END) AS `Gênero / Gender`"),
								DB::raw('country_name AS `País / Country`'),
								DB::raw('job_title AS `Título o Cargo / Title or Position`'),
								DB::raw('institution_type AS `Tipo de institución / Type of institution`'),
								DB::raw('company AS `Empresa o Organización / Company or Organization`'),
								DB::raw('origin AS `Fuente del Contacto / Source dataset`'),
								DB::raw('twitter AS `Twitter`'),
								DB::raw('projects AS `Productos de interés / Products of interest`'),
								DB::raw('topics AS `Temas de interése / Topics of interest`'),
								DB::raw('phone AS `Número de Teléfono / Phone Number`'),
								DB::raw('language AS `Idioma / Language`')
							)
						)
						->take($process_max)
						->get();

		foreach ( $data as $user ) {
			$downloaded_user[] = $user->id;
		}

		if ( isset($downloaded_user) && !empty($downloaded_user) )
			Contact::whereIn('id', $downloaded_user)->update(array('is_downloaded' => '1'));


		$total_files_to_download = 0;

		if ( Session::has('files_to_download') ) {
			$file_path = Session::get('files_to_download');
			$total_files_to_download = count($file_path);
		}


		$filename = Contact::select('origin')->first();

		// $full_filename = $filename->origin.' '.date('m-d-Y').' - Normalized '.substr(sha1(date('sih dmY').rand(0,99999999)),0,5);
		$full_filename = $filename->origin.' '.date('m-d-Y').' - Normalized ('.count($downloaded_user).' contacts) ('.($total_files_to_download+1).')';


		$file_path[] = asset('files').'/'.$full_filename;
		Session::put('files_to_download',$file_path);


		Excel::create( $full_filename, function($excel) use ($data) {

			$excel->setTitle('Inter-American Development Bank');
			$excel->setCreator('W3 Inova')
				  ->setCompany('w3inova.com');

			$excel->sheet('Contacts', function($sheet) use ($data) {

				$sheet->setOrientation('landscape');
        		$sheet->fromArray($data, null, 'A1', false, true);

    		});

		// })->download('csv');
		})->store('csv', public_path('files'));

		return Redirect::to('/#step5')->with('alert','File saved. Click on list to download.');

	}

	public function importIntoMailchimp() {

		// Test Lucas Moreira
		// $mc_settings = array(
		// 	'server' => 'us9',
		// 	'api_key' => 'fcfe5a01b0030d075172a9fcee0a976a-us9',
		// 	'list_id' => 'b06ba31312',
		//  'authorization' => 'dGVzdGU6MTU1OTJmZTIxYWYxZWIzZDRlZDc5OGFkZTNhYTc3OTgtdXMxMA=='
		// );

		// Production
		$mc_settings = array(
			'server' => 'us10',
			'api_key' => '15592fe21af1eb3d4ed798ade3aa7798-us10',
			'list_id' => 'adfbacfd19',
			'authorization' => 'dGVzdGU6MTU1OTJmZTIxYWYxZWIzZDRlZDc5OGFkZTNhYTc3OTgtdXMxMA=='
		);

		$mc_api_endpoint = 'https://'.$mc_settings['server'].'.api.mailchimp.com/3.0';

		$mc_count_subscribed = 0;
		$mc_count_updated = 0;

		// Check if member exists
		// If NOT exist
		// Insert member with POST
		// Else
		// Update member with PATCH

		$member = Contact::where('is_synced','=',0)
						  ->take(100) // 500 is max per time - Defined by MailChimp
						  ->get();

		$curl = curl_init();

		foreach ( $member as $member ) :

			// Checking if contact exist on Mailchimp

			// $curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $mc_api_endpoint."/lists/".$mc_settings['list_id']."/members/".$member->email_hash,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 3600,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_POSTFIELDS => "",
				CURLOPT_HTTPHEADER => array(
					"authorization: Basic ".$mc_settings['authorization'],
					"cache-control: no-cache",
					"postman-token: c15ee598-6c82-6b06-be6e-04b5b82a9f57"
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			// curl_close($curl);

			if ( $err ) :

				echo "cURL Error #:" . $err;

			else :

				$array = json_decode($response);
				$contact_status = $array->status;

				if ( $contact_status == '404' ) {

					// Insert contact on Mailchimp

					$curl = curl_init();

					$the_gender = '';
					if ( !isset($member->gender) || empty($member->gender) || $member->gender == ''  )
						$the_gender = '';
					elseif ( $member->gender == 'm' )
						$the_gender = 'Masculino / Male';
					elseif ( $member->gender == 'f' )
						$the_gender = 'Feminino / Female';
					else
						$the_gender = 'Otro / Other';

					// To insert into groups
					// Now language is MMERGE14
					/*
					if ( $member->language == 'English' )
						$the_language = array('b751f6fbfa' => true);
					elseif ( $member->language == 'Spanish' )
						$the_language = array('7455fda3c8' => true);
					elseif ( $member->language == 'French' )
						$the_language = array('f5db18593f' => true);
					elseif ( $member->language == 'Portuguese' )
						$the_language = array('8546d08830' => true);
					*/

					$insert_data = array(
						'email_address' => $member->email,
						'status' => 'subscribed',
						'merge_fields' => array(
							'MERGE0' => $member->email,// EMAIL
							'FNAME' => $member->first_name,// FNAME
							'LNAME' => $member->last_name,// LNAME
							'MMERGE3' => $member->birth_date,// MMERGE3 (birth date)
							'MMERGE4' => $the_gender,// MMERGE4 (gender)
							'MMERGE5' => $member->country_name,// MMERGE5 (country)
							'MMERGE6' => $member->job_title,// MMERGE6 (title/position)
							'MMERGE7' => $member->company,// MMERGE7 (company)
							'MMERGE8' => $member->institution_type,// MMERGE8 (institution type)
							'MMERGE9' => $member->origin,// MMERGE9 (origin)
							'MMERGE10' => $member->twitter,// MMERGE10 (Twitter)
							// 'MMERGE11' => $member->projects,// MMERGE11 (Interests) - TEXT fields
							// 'MMERGE12' => $member->topics,// MMERGE12 (Interests too) - TEXT fields
							'MMERGE13' => $member->phone,// MMERGE13 (Phone)
							'MMERGE14' => $member->language
						),
						// These fields will be used later
						// The correct way is: 'id' => BOOLEAN
						// Getting IDs: /lists/{list_id}/interest-categories/{interests-id}/interests
						// http://stackoverflow.com/questions/38613273/cant-add-user-to-a-group-using-mailchimp-api-3-0
						// See the file in Dropbox
						// 'interests' => array(
							// '187ad164ee' => $member->projects, // MMERGE11 (Productos de interés / Products of Interest) - CHECKBOXES fields
							// '884941fbcb' => $member->topics, // MMERGE12 (Temas de interés / Themes of Interest) - CHECKBOXES fields
							// 'bab88f616a' => $member->language // Language
						// )
						// 'interests' => $the_language
					);

					$update_this[] = $member->email;

					$insert_data = json_encode($insert_data);

					curl_setopt_array($curl, array(
						CURLOPT_URL => $mc_api_endpoint."/lists/".$mc_settings['list_id']."/members",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 3600,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => $insert_data,
						CURLOPT_HTTPHEADER => array(
							"authorization: Basic ".$mc_settings['authorization'],
							"cache-control: no-cache",
							"postman-token: b80e28d3-6f4d-ec51-cf6c-e96bc70a8283"
						),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					// curl_close($curl);

					if ( $err ) {

						echo "cURL Error #:" . $err;

					} else {

						// Contact INSERTED on Mailchimp
						++$mc_count_subscribed;

					}

				} else {

					// Contact already subscribed on MC
					$update_this_already_subscribed[] = $member->email;

				}

			endif;

			// $member->is_synced = 1;
			// $member->save();

		endforeach;

		curl_close($curl);

		if ( isset($update_this) && !empty($update_this) )
			Contact::whereIn('email',$update_this)->update(array('is_synced' => 1));

		if ( isset($update_this_already_subscribed) && !empty($update_this_already_subscribed) )
			Contact::whereIn('email',$update_this_already_subscribed)->update(array('is_synced' => 1));

		$stats = Stat::where('id','=',1)->first();

		if ( isset($update_this) && !empty($update_this) )
			$stats->subscribed = $stats->subscribed + $mc_count_subscribed;

		if ( isset($update_this_already_subscribed) && !empty($update_this_already_subscribed) )
			$stats->updated = $stats->updated + count($update_this_already_subscribed);

		$stats->save();

		return Redirect::back()->with('alert',$mc_count_subscribed.' contacts syncronized.');

	}

}
